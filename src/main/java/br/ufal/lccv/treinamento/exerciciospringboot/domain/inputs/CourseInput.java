package br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@Schema(description = "Representação de entrada de um novo curso.")
public class CourseInput {

    @Schema(description = "Nome do curso.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Ano de criação do curso.")
    private Integer creationYear;

    @Schema(description = "Prédio de lencionamento do curso.")
    private String building;

    @Schema(description = "ID do coordenador do curso.")
    private UUID coordinatorId;
}
