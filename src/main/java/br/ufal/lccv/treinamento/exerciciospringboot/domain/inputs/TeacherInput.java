package br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Schema(description = "Representação de entrada de um novo professor.")
public class TeacherInput {
    
    @Schema(description = "CPF do professor.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$")
    private String cpf;

    @Schema(description = "Nome do professor.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Titulação do professor.")
    private String titration;

}
