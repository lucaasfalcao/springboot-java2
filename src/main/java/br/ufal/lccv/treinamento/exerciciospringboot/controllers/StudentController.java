package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.ClassBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.ClassDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.StudentInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Class;
import br.ufal.lccv.treinamento.exerciciospringboot.services.StudentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Students", description = "Endpoints da entidade Student")
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentController {

    private final StudentService service;

    @Operation(
            summary = "Criar novo estudante",
            description = "Baseado no corpo da requisição, adiciona um estudante no banco de dados."
    )
    @PostMapping("/")
    public StudentDTO create(@Valid @RequestBody StudentInput studentInput) {
        Student student = service.create(studentInput);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Obter a lista de todos os estudantes",
        description = "Retorna uma lista com os dados de todos os estudantes cadastrados."
    )
    @GetMapping
    public List<StudentDTO> findAll() {
        List<Student> allStudents = service.findAll();
        return StudentBuilder.build(allStudents);
    }

    @Operation(
        summary = "Obter os dados de um estudante",
        description = "Baseado no ID requerido, retorna os dados do estudante."
    )
    @GetMapping("/{id}")
    public StudentDTO findOne(@PathVariable UUID id) {
        Student student = service.findOne(id);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Obter o curso de um estudante",
        description = "Baseado no ID requerido, retorna os dados do curso do estudante."
    )
    @GetMapping("/{id}/course")
    public CourseDTO findStudentCourse(@PathVariable UUID id) {
        Course course = service.findStudentCourse(id);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Atualizar os dados de um estudante",
        description = "Baseado no ID e no corpo da requisição, atualiza os dados de um estudante."
    )
    @PutMapping("/{id}")
    public StudentDTO update(@PathVariable UUID id, @Valid @RequestBody StudentInput studentInput) {
        Student student = service.update(id, studentInput);
        return StudentBuilder.build(student);
    }

    @Operation(
        summary = "Remover estudante",
        description = "Baseado no ID requerido, remove um estudante do banco de dados."
    )
    @DeleteMapping("/{id}")
    public StudentDTO delete(@PathVariable UUID id) {
        Student student = service.delete(id);
        return StudentBuilder.build(student);
    }
    
    @Operation(
            summary = "Matricular estudante em uma disciplina",
            description = "Baseado nos IDs inseridos, matricula um estudante em uma disciplina."
    )
    @PostMapping("/{id}/class/{classId}")
    public ClassDTO postClass(@PathVariable UUID id, @PathVariable UUID classId) {
        Class classe = service.postClass(id, classId);
        return ClassBuilder.build(classe);
    }
}
