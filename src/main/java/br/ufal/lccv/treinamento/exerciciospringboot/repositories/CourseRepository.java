package br.ufal.lccv.treinamento.exerciciospringboot.repositories;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface CourseRepository extends JpaRepository<Course, UUID> {
    
}
