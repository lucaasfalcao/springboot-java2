package br.ufal.lccv.treinamento.exerciciospringboot.domain.builders;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;

import java.util.LinkedList;
import java.util.List;

public class CourseBuilder {
    
    public static CourseDTO build(Course course) {
        return CourseDTO.builder()
                .id(course.getId())
                .name(course.getName())
                .creationYear(course.getCreationYear())
                .building(course.getBuilding())
                .build();
    }

    public static Course build(CourseInput input) {
        return Course.builder()
                .name(input.getName())
                .creationYear(input.getCreationYear())
                .building(input.getBuilding())
                .build();
    }

    public static List<CourseDTO> build(List<Course> courses) {
        List<CourseDTO> finalList = new LinkedList<>();
        for (Course course : courses) {
            CourseDTO courseDTO = build(course);
            finalList.add(courseDTO);
        }
        return finalList;
    }
}
