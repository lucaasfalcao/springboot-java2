package br.ufal.lccv.treinamento.exerciciospringboot.domain.builders;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.ClassDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.ClassInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Class;

import java.util.LinkedList;
import java.util.List;

public class ClassBuilder {
    
    public static ClassDTO build(Class classe) {
        return ClassDTO.builder()
                .id(classe.getId())
                .name(classe.getName())
                .description(classe.getDescription())
                .code(classe.getCode())
                .build();
    }

    public static Class build(ClassInput input) {
        return Class.builder()
                .name(input.getName())
                .description(input.getDescription())
                .code(input.getCode())
                .build();
    }

    public static List<ClassDTO> build(List<Class> classes) {
        List<ClassDTO> finalList = new LinkedList<>();
        for (Class classe : classes) {
            ClassDTO classDTO = build(classe);
            finalList.add(classDTO);
        }
        return finalList;
    }
}
