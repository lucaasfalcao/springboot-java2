package br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.UUID;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Schema(description = "Representação de entrada de um novo estudante.")
public class StudentInput {
    
    @Schema(description = "CPF do estudante.")
    @NotBlank(message = "CPF não pode ser vazio!")
    @Pattern(regexp = "^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$")
    private String cpf;

    @Schema(description = "Nome do estudante.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Idade do estudante.")
    @Min(value = 18, message = "Precisa ser maior de idade.")
    private Integer age;

    @Schema(description = "ID do curso do estudante.")
    private UUID courseId;
}
