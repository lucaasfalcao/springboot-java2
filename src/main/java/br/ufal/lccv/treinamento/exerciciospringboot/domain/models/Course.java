package br.ufal.lccv.treinamento.exerciciospringboot.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Course {
    
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type ="uuid-char")
    private UUID id;

    private String name;

    private Integer creationYear;

    private String building;

    @OneToMany(mappedBy = "course")
    private List<Student> students;

    @OneToOne
    private Teacher coordinator;

}
