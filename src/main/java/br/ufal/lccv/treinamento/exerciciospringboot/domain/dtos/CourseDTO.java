package br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Schema(description = "Representação de um curso.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO {
    
    @Schema(description = "ID do curso.")
    private UUID id;

    @Schema(description = "Nome do curso.")
    private String name;

    @Schema(description = "Ano de criação do curso.")
    private Integer creationYear;

    @Schema(description = "Prédio do curso.")
    private String building;

}
