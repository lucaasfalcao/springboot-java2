package br.ufal.lccv.treinamento.exerciciospringboot.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Class;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.ClassBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.ClassInput;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.ClassRepository;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClassService {

    private final ClassRepository classRepository;
    private final TeacherRepository teacherRepository;

    private List<Class> classes = new LinkedList<>();

    public Class create(ClassInput classInput) {
        log.info("Disciplina que veio: {}", classInput);

        Class classe = ClassBuilder.build(classInput);

        Teacher teacher = teacherRepository.getReferenceById(classInput.getTeacherId());
        classe.setTeacher(teacher);

        boolean hasClass = classes.stream().anyMatch((c) -> {
            return c.getName().equals(classInput.getName());
        });
        if (hasClass) {
            throw new RuntimeException("Disciplina já existe!");
        }
        classes.add(classe);

        classe = classRepository.save(classe);

        return classe;
    }

    public List<Class> findAll() {
        List<Class> allClasses = classRepository.findAll();
        return allClasses;
    }

    public Class findOne(UUID id) {
        Class classe = classRepository.getReferenceById(id);
        return classe;
    }

    public Teacher findClassTeacher(UUID id) {
        Class classe = classRepository.getReferenceById(id);
        Teacher teacher = classe.getTeacher();
        return teacher;
    }

    
    public Class update(UUID id, ClassInput input) {
    
        Class classe = classRepository.getReferenceById(id);
        UUID teacherId = input.getTeacherId();
        Teacher teacher = teacherRepository.getReferenceById(teacherId);

        classe.setName(input.getName());
        classe.setDescription(input.getDescription());
        classe.setCode(input.getCode());
        classe.setTeacher(teacher);
        
        classRepository.save(classe);
        return classe;
    }

    public List<Student> findAllStudents(UUID id) {
        Class classe = classRepository.getReferenceById(id);
        List<Student> allStudents = classe.getStudents();
        return allStudents;
    }
}
