package br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
@Schema(description = "Representação de entrada de uma nova disciplina.")
public class ClassInput {

    @Schema(description = "Nome da disciplina.")
    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    @Schema(description = "Descrição da disciplina.")
    private String description;

    @Schema(description = "Código da disciplina.")
    private String code;

    @Schema(description = "ID do professor da disciplina.")
    private UUID teacherId;
}

