package br.ufal.lccv.treinamento.exerciciospringboot.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.CourseRepository;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseService {
    
    private final CourseRepository courseRepository;
    private final TeacherRepository teacherRepository;

    private List<Course> courses = new LinkedList<>();

    public Course create(CourseInput courseInput) {
        log.info("Curso que veio: {}", courseInput);

        Course course = CourseBuilder.build(courseInput);

        Teacher coordinator = teacherRepository.getReferenceById(courseInput.getCoordinatorId());
        course.setCoordinator(coordinator);

        boolean hasCourse = courses.stream().anyMatch((c) -> {
            return c.getName().equals(courseInput.getName());
        });
        if (hasCourse) {
            throw new RuntimeException("Curso já existe!");
        }
        courses.add(course);
        
        course = courseRepository.save(course);

        return course;
    }

    public List<Course> findAll() {
        List<Course> allCourses = courseRepository.findAll();
        return allCourses;
    }

    public Course findOne(UUID id) {
        Course course = courseRepository.getReferenceById(id);
        return course;
    }

    public Teacher findCourseCoordinator(UUID id) {
        Course course = courseRepository.getReferenceById(id);
        Teacher coordinator = course.getCoordinator();
        return coordinator;
    }

    public Course update(UUID id, CourseInput input) {
    
        Course course = courseRepository.getReferenceById(id);
        UUID coordinatorId = input.getCoordinatorId();
        Teacher coordinator = teacherRepository.getReferenceById(coordinatorId);

        course.setName(input.getName());
        course.setCreationYear(input.getCreationYear());
        course.setBuilding(input.getBuilding());
        course.setCoordinator(coordinator);
        
        courseRepository.save(course);
        return course;
    }

    public List<Student> findAllStudents(UUID id) {
        Course course = courseRepository.getReferenceById(id);
        List<Student> allStudents = course.getStudents();
        return allStudents;
    }
}
