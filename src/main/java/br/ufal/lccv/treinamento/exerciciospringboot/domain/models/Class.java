package br.ufal.lccv.treinamento.exerciciospringboot.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;

import java.util.List;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Class {
    
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Type(type ="uuid-char")
    private UUID id;

    @NotBlank(message = "Nome não pode ser vazio!")
    private String name;

    private String description;
    
    private String code;

    @ManyToMany
    @JoinTable(name = "student_classes", joinColumns = @JoinColumn(name = "class_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"))
    private List<Student> students;

    @OneToOne
    private Teacher teacher;
}
