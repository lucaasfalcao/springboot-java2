package br.ufal.lccv.treinamento.exerciciospringboot.domain.builders;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.TeacherDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.TeacherInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;

import java.util.LinkedList;
import java.util.List;

public class TeacherBuilder {
    
    public static TeacherDTO build(Teacher teacher) {
        return TeacherDTO.builder()
                .id(teacher.getId())
                .cpf(teacher.getCpf())
                .name(teacher.getName())
                .titration(teacher.getTitration())
                .build();
    }

    public static Teacher build(TeacherInput input) {
        return Teacher.builder()
                .cpf(input.getCpf())
                .name(input.getName())
                .titration(input.getTitration())
                .build();
    }

    public static List<TeacherDTO> build(List<Teacher> teachers) {
        List<TeacherDTO> finalList = new LinkedList<>();
        for (Teacher teacher : teachers) {
            TeacherDTO teacherDTO = build(teacher);
            finalList.add(teacherDTO);
        }
        return finalList;
    }
}
