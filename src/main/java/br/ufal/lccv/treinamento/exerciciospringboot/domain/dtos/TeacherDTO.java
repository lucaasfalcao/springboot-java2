package br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Schema(description = "Representação de um professor.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeacherDTO {
    
    @Schema(description = "ID do professor.")
    private UUID id;

    @Schema(description = "CPF do professor.")
    private String cpf;

    @Schema(description = "Nome do professor.")
    private String name;

    @Schema(description = "Titulação do professor.")
    private String titration;
}
