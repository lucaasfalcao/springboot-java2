package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.CourseBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.TeacherBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.CourseDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.TeacherDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.CourseInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.services.CourseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Courses", description = "Endpoints da entidade Course")
@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CourseController {

    private final CourseService service;

    @Operation(
            summary = "Criar novo curso",
            description = "Baseado no corpo da requisição, adiciona um curso no banco de dados."
    )
    @PostMapping("/")
    public CourseDTO create(@Valid @RequestBody CourseInput courseInput) {
        Course course = service.create(courseInput);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Obter a lista de todos os cursos",
        description = "Retorna uma lista com os dados de todos os cursos cadastrados."
    )
    @GetMapping
    public List<CourseDTO> findAll() {
        List<Course> allCourses = service.findAll();
        return CourseBuilder.build(allCourses);
    }

    @Operation(
        summary = "Obter os dados de um curso",
        description = "Baseado no ID requerido, retorna os dados do curso."
    )
    @GetMapping("/{id}")
    public CourseDTO findOne(@PathVariable UUID id) {
        Course course = service.findOne(id);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Obter o coordenador de um curso",
        description = "Baseado no ID requerido, retorna os dados do coordenador do curso."
    )
    @GetMapping("/{id}/coordinator")
    public TeacherDTO findCourseCoordinator(@PathVariable UUID id) {
        Teacher coordinator = service.findCourseCoordinator(id);
        return TeacherBuilder.build(coordinator);
    }

    @Operation(
        summary = "Atualizar os dados de um curso",
        description = "Baseado no ID e no corpo da requisição, atualiza os dados de um curso."
    )
    @PutMapping("/{id}")
    public CourseDTO update(@PathVariable UUID id, @Valid @RequestBody CourseInput courseInput) {
        Course course = service.update(id, courseInput);
        return CourseBuilder.build(course);
    }

    @Operation(
        summary = "Obter a lista de todos os alunos do curso",
        description = "Retorna uma lista com os dados de todos os alunos cadastrados no curso."
    )
    @GetMapping("/{id}/students")
    public List<StudentDTO> findAllStudents(@PathVariable UUID id) {
        List<Student> allStudents = service.findAllStudents(id);
        return StudentBuilder.build(allStudents);
    }
}
