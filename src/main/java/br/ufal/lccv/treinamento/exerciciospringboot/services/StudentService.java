package br.ufal.lccv.treinamento.exerciciospringboot.services;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.StudentInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Course;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Class;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.CourseRepository;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.StudentRepository;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.ClassRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StudentService {
    
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final ClassRepository classRepository;

    private List<Student> students = new LinkedList<>();

    public Student create(StudentInput studentInput) {
        log.info("Estudante que veio: {}", studentInput);

        Student student = StudentBuilder.build(studentInput);

        Course course = courseRepository.getReferenceById(studentInput.getCourseId());
        student.setCourse(course);

        boolean hasStudent = students.stream().anyMatch((s) -> {
            return s.getCpf().equals(studentInput.getCpf());
        });
        if (hasStudent) {
            throw new RuntimeException("Estudante já existe!");
        }
        students.add(student);
        
        student = studentRepository.save(student);

        return student;
    }

    public List<Student> findAll() {
        List<Student> allStudents = studentRepository.findAll();
        return allStudents;
    }

    public Student findOne(UUID id) {
        Student student = studentRepository.getReferenceById(id);
        return student;
    }

    public Student update(UUID id, StudentInput input) {
    
        Student student = studentRepository.getReferenceById(id);
        UUID courseId = input.getCourseId();
        Course course = courseRepository.getReferenceById(courseId);

        student.setAge(input.getAge());
        student.setCpf(input.getCpf());
        student.setName(input.getName());
        student.setCourse(course);
        
        studentRepository.save(student);
        return student;
    }

    public Student delete(UUID id) {
        Student student = studentRepository.getReferenceById(id);

        studentRepository.delete(student);
        return student;
    }

    public Course findStudentCourse(UUID id) {
        Student student = studentRepository.getReferenceById(id);
        Course course = student.getCourse();
        return course;
    }

    public Class postClass(UUID id, UUID classId) {

        Student student = studentRepository.getReferenceById(id);
        Class classe = classRepository.getReferenceById(classId);
        
        List<Class> classes = student.getClasses();
        classes.add(classe);

        studentRepository.save(student);
        return classe;
    }

}
