package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.ClassBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.StudentBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.TeacherBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.ClassDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.TeacherDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.StudentDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.ClassInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Class;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Student;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.services.ClassService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Classes", description = "Endpoints da entidade Class")
@RestController
@RequestMapping("/classes")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ClassController {
    private final ClassService service;

    @Operation(
            summary = "Criar nova disciplina",
            description = "Baseado no corpo da requisição, adiciona uma disciplina no banco de dados."
    )
    @PostMapping("/")
    public ClassDTO create(@Valid @RequestBody ClassInput classInput) {
        Class classe = service.create(classInput);
        return ClassBuilder.build(classe);
    }

    @Operation(
        summary = "Obter a lista de todas as disciplinas",
        description = "Retorna uma lista com os dados de todas as disciplinas cadastradas."
    )
    @GetMapping
    public List<ClassDTO> findAll() {
        List<Class> allClasses = service.findAll();
        return ClassBuilder.build(allClasses);
    }

    @Operation(
        summary = "Obter os dados de uma disciplina",
        description = "Baseado no ID requerido, retorna os dados da disciplina."
    )
    @GetMapping("/{id}")
    public ClassDTO findOne(@PathVariable UUID id) {
        Class classe = service.findOne(id);
        return ClassBuilder.build(classe);
    }

    @Operation(
        summary = "Obter o professor de uma disciplina",
        description = "Baseado no ID requerido, retorna os dados do professor da disciplina."
    )
    @GetMapping("/{id}/teacher")
    public TeacherDTO findClassTeacher(@PathVariable UUID id) {
        Teacher teacher = service.findClassTeacher(id);
        return TeacherBuilder.build(teacher);
    }

    @Operation(
        summary = "Atualizar os dados de uma disciplina",
        description = "Baseado no ID e no corpo da requisição, atualiza os dados de uma disciplina."
    )
    @PutMapping("/{id}")
    public ClassDTO update(@PathVariable UUID id, @Valid @RequestBody ClassInput classInput) {
        Class classe = service.update(id, classInput);
        return ClassBuilder.build(classe);
    }

    @Operation(
        summary = "Obter a lista de todos os alunos da disciplina",
        description = "Retorna uma lista com os dados de todos os alunos cadastrados na disciplina."
    )
    @GetMapping("/{id}/students")
    public List<StudentDTO> findAllStudents(@PathVariable UUID id) {
        List<Student> allStudents = service.findAllStudents(id);
        return StudentBuilder.build(allStudents);
    }
}
