package br.ufal.lccv.treinamento.exerciciospringboot.controllers;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.TeacherBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos.TeacherDTO;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.TeacherInput;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.services.TeacherService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@Tag(name = "Teachers", description = "Endpoints da entidade Teacher")
@RestController
@RequestMapping("/teachers")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeacherController {

    private final TeacherService service;

    @Operation(
        summary = "Criar novo professor",
        description = "Baseado no corpo da requisição, adiciona um professor no banco de dados."
    )
    @PostMapping("/")
    public TeacherDTO create(@Valid @RequestBody TeacherInput teacherInput) {
        Teacher teacher = service.create(teacherInput);
        return TeacherBuilder.build(teacher);
    }

    @Operation(
        summary = "Obter a lista de todos os professores",
        description = "Retorna uma lista com os dados de todos os professores cadastrados."
    )
    @GetMapping
    public List<TeacherDTO> findAll() {
        List<Teacher> allTeachers = service.findAll();
        return TeacherBuilder.build(allTeachers);
    }

    @Operation(
        summary = "Obter os dados de um professor",
        description = "Baseado no ID requerido, retorna os dados do professor."
    )
    @GetMapping("/{id}")
    public TeacherDTO findOne(@PathVariable UUID id) {
        Teacher teacher = service.findOne(id);
        return TeacherBuilder.build(teacher);
    }
}
