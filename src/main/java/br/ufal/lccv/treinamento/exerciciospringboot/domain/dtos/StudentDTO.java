package br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Schema(description = "Representação de um estudante.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {
    
    @Schema(description = "ID do estudante.")
    private UUID id;

    @Schema(description = "CPF do estudante.")
    private String cpf;

    @Schema(description = "Nome do estudante.")
    private String name;

    @Schema(description = "Idade do estudante.")
    private Integer age;
}
