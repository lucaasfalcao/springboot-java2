package br.ufal.lccv.treinamento.exerciciospringboot.domain.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Schema(description = "Representação de um curso.")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClassDTO {
    
    @Schema(description = "ID da disciplina.")
    private UUID id;

    @Schema(description = "Nome da disciplina.")
    private String name;

    @Schema(description = "Descrição da disciplina.")
    private String description;

    @Schema(description = "Código da disciplina.")
    private String code;

}
