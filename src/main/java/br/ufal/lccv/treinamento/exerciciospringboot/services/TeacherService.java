package br.ufal.lccv.treinamento.exerciciospringboot.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import br.ufal.lccv.treinamento.exerciciospringboot.domain.models.Teacher;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.builders.TeacherBuilder;
import br.ufal.lccv.treinamento.exerciciospringboot.domain.inputs.TeacherInput;
import br.ufal.lccv.treinamento.exerciciospringboot.repositories.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TeacherService {
    
    private final TeacherRepository teacherRepository;

    private List<Teacher> teachers = new LinkedList<>();

    public Teacher create(TeacherInput teacherInput) {
        log.info("Professor que veio: {}", teacherInput);

        Teacher teacher = TeacherBuilder.build(teacherInput);

        boolean hasTeacher = teachers.stream().anyMatch((t) -> {
            return t.getCpf().equals(teacherInput.getCpf());
        });
        if (hasTeacher) {
            throw new RuntimeException("Professor já existe!");
        }
        teachers.add(teacher);

        teacher = teacherRepository.save(teacher);

        return teacher;
    }

    public List<Teacher> findAll() {
        List<Teacher> allTeachers = teacherRepository.findAll();
        return allTeachers;
    }

    public Teacher findOne(UUID id) {
        Teacher teacher = teacherRepository.getReferenceById(id);
        return teacher;
    }
}
